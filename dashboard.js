// Set account variables
const username = localStorage.getItem("username")
const email = localStorage.getItem("email")
const password = localStorage.getItem("password")

function start() {
    // Redirect if not logged in
    if (!username || !email || !password) {
        window.location.href = "./index.html";
        return;
    }

    // Add info to document
    document.getElementById("greeting").innerHTML = `Hello ${username}!`;
    document.getElementById("email").innerHTML += email;
    document.getElementById("password").innerHTML += password;
}

function logOut() {
    // Remove info from storage
    localStorage.removeItem("username");
    localStorage.removeItem("email");
    localStorage.removeItem("password");
    
    // Redirect to index page
    window.location.href = "./index.html";
    return;
}