function returnError(status, response) {
    console.log({status, response})
}

function handleSignUp() {
    var form = document.getElementById("signupform");

    // Define variables
    const email = document.getElementsByName("email")[0].value;
    const username = document.getElementsByName("username")[0].value;
    const password = document.getElementsByName("password")[0].value;
    const passwordconfirm = document.getElementsByName("passwordconfirm")[0].value;

    // Make sure all fields are filled out.
    if ((!email || email == "") || (!username || username == "") || (!password || password == "") || (!passwordconfirm || passwordconfirm == "")) {
        returnError("1", "Missing information.");
        return 1;
    }

    // Make sure passwords match
    if (password !== passwordconfirm) {
        returnError("1", "Passwords don't match.");
        return 1;
    }

    // Store user information
    localStorage.setItem("email", email);
    localStorage.setItem("username", username);
    localStorage.setItem("password", password);

    // Redirect to dashboard
    window.location.href = "./dashboard.html"
}